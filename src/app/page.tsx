import Image from "next/image";
import PartOneHomePage from "./Component/PartOneHomePage";
import PartOneHome from "./Component/PartTwoHome";

export default function Home() {
  return (
    <div className="">
      <PartOneHomePage />
      <PartOneHome />
    </div>
  );
}

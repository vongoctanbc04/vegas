/* eslint-disable react/no-unescaped-entities */
"use client"
import { useEffect, useState } from 'react';
import AOS from 'aos';
import 'aos/dist/aos.css';
import Image from 'next/image';
import logo from "@/app/Image/logoVegas.png"
import { montserrat } from '@/app/Style/fonts';
import logoEng from "@/app/Image/Vector 66.png"

import mnMb from "@/app/Image/burger-menu-color.svg"
import mnMb2 from "@/app/Image/mnMB2.png"

const HeaderPage = () => {

    // Dùng để chỉnh hiệu ứng từ trên xuất hiện
    useEffect(() => {
        AOS.init({
            once: true,
            duration: 1000,
            offset: 50,
            easing: "ease",
        });
    }, []);

    const SccItem = 'content-center'

    // Quay hình icon song ngữ
    const [isRotated, setIsRotated] = useState(false);
    const handleClick = () => {
        setIsRotated(!isRotated);
    };

    const [bilingual, setBilingual] = useState("En")
    const handleVi = () => {
        setBilingual("Vi")
        setIsRotated(false)
    }
    const handleEn = () => {
        setBilingual("En")
        setIsRotated(false)
    }


    const [tan, setTan] = useState(true)
    return (
        <div className='border-b-2 border-[#9d7b58] sticky top-0 z-10 bg-white content-center'>
            <div data-aos="fade-down" className={`${montserrat.className} w-[80%] m-auto my-4 text-[#9d7b58]`}>
                <div className='flex justify-between'>
                    <div>
                        <Image src={logo} alt='logo' width={80} height={28} />
                    </div>
                    <div className='hidden tan:block'>
                        <div className='flex gap-8'>
                            <div className={SccItem}>ABOUT US</div>
                            <div className={SccItem}>PLAY</div>
                            <div className={SccItem}>DINE</div>
                            <div className={SccItem}>STAY</div>
                            <div className={SccItem}>WHAT'S ON</div>
                            <div className={SccItem}>CONTACT</div>
                        </div>
                    </div>
                    <div className='flex gap-4'>
                        <div className='w-[38px]'>
                            <div className='flex gap-2' onClick={handleClick}>
                                <div className='font-normal'>{bilingual}</div>
                                <Image src={logoEng} alt='logo' className={`h-fit m-auto transition-transform duration-500 ${isRotated ? 'rotate-180' : ''
                                    }`} />
                            </div>
                            {isRotated && <div className="absolute z-10 bg-white w-[42px] shadow-[0_0_10px_rgba(0,0,0,0.5)]">
                                <div className='pt-2 text-center hover:bg-[#C6B392]' onClick={handleVi}>Vi</div>
                                <div className='py-2 text-center hover:bg-[#C6B392]' onClick={handleEn}>En</div>
                            </div>}

                        </div>
                        <div className='content-center w-[30px]' onClick={() => setTan(!tan)}>
                            <Image src={tan ? mnMb : mnMb2} alt='logo' />
                        </div>
                    </div>
                </div>
            </div>
        </div>


    )
}
export default HeaderPage
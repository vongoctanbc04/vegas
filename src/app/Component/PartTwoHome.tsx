import Image from "next/image"
import vegasBottom from "../Image/vegas-bottom.svg"
import vegasTop from "../Image/vegas-top.svg"


const PartOneHome = () => {

    // Làm màu chữ có 2 màu trên dưới
    const cssText2MauChu = 'bg-gradient-to-b from-[#634a3a] to-[#be8a25] bg-clip-text text-transparent'


    return <div>
        <div className="text-center pt-8">
            <div className={`text-5xl font-medium ${cssText2MauChu}`}>VEGAS CLUB</div>
            <div className="pt-3 text-2xl text-[#a57416]">ENDLESS OPPORTUNITIES - ENDLESS REWARDS - ENDLESS FUN</div>
        </div>
        <div className="flex pt-12 w-[70%] m-auto">
            <div className="w-[67%] shadow-[0px_0px_60px_rgba(196,179,144,0.5)]">
                <iframe
                    src="https://www.youtube.com/embed/Ia7ARYml50o?autoplay=1&mute=0&controls=0&origin=https%3A%2F%2Fvegas.appbeta.info&playsinline=1&showinfo=0&rel=0&iv_load_policy=3&modestbranding=1&enablejsapi=1&widgetid=1&vq=small"
                    width="100%"
                    height="500"
                    frameBorder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                    referrerPolicy="strict-origin-when-cross-origin"
                    allowFullScreen
                ></iframe>
            </div>
            <div className="w-[32%] min-h-[500px] flex flex-col ">
                <div>
                    <Image src={vegasTop} alt="vegasTop" />
                </div>
                <div className="flex-1 border-l-[1px] border-[#f4e7d4] ml-[24px] text-[#9d816a] text-sm flex">
                    <div className="w-[80%] m-auto">Situated at Caravelle Saigon Hotel, one of the most classy 5-star hotels in Ho Chi Minh city, Vegas Club offers a dazzling array of e-gaming selections in a vibrant and sophisticated setting to provide an unparalleled e-gaming experiences.</div>
                </div>
                <div>
                    <Image src={vegasBottom} alt="vegasBottom" />
                </div>
            </div>
        </div>
    </div>
}
export default PartOneHome


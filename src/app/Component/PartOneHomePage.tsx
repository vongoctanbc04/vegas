'use client';
import ImageSlider from "../Image/h2.webp"
import ImageBorder from "../Image/boder.png"
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import Image from "next/image";
// import { FaChevronLeft, FaChevronRight } from "react-icons/fa";
import { useEffect } from "react";
import AOS from 'aos';
import 'aos/dist/aos.css';

interface DataSlider {
    Image: any,
    Content: string,
    Name: string,
    Color: string,
}

const PartOneHomePage = () => {

    // Dùng để chỉnh hiệu ứng từ trên xuất hiện
    useEffect(() => {
        AOS.init({
            once: true,
            duration: 1000,
            offset: 50,
            easing: "ease",
        });
    }, []);

    const DataSlider: DataSlider[] = [
        {
            Image: ImageSlider,
            Content: "EXCITING RANGE OF ",
            Name: "Take advantage of a wide variety of members-only offers that make every visit more rewarding wide variety of members-only offers",
            Color: "LATEST GAMES",
        },
        {
            Image: ImageSlider,
            Content: "A COMMUNITY ",
            Name: "Where you can have wonderful gaming experience, enjoy the excitement and share the fun",
            Color: "HUB",
        },
        {
            Image: ImageSlider,
            Content: "EXCLUSIVE MEMBERS-ONLY TREATMENTS ",
            Name: "Take advantage of a wide variety of members-only offers that make every visit more rewarding",
            Color: "TREATMENTS",
        },

    ]

    const settings = {
        // dots: true,
        infinite: true,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
    };


    return (
        <div>
            <div className="overflow-x-hidden">
                <Slider {...settings} >
                    {DataSlider.map(slide => (
                        <div key={slide.Name} className="relative ">
                            <Image src={slide.Image} alt="slide" className="h-[500px] " />
                            <div className="absolute top-0 left-0 w-full h-full bg-[rgba(46,23,6,0.6)]"></div>
                            <div className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 z-10 w-1/2 text-white">
                                <div className="w-full text-center text-4xl font-medium ">
                                    <p data-aos="fade-up" >
                                        <span >{slide.Content}</span>
                                        <span className="text-[rgb(255,217,102)]">{slide.Color}</span>
                                    </p>
                                </div>
                                <div data-aos="fade-up" className="w-[40%] text-center m-auto pt-4" >{slide.Name}</div>
                            </div>

                        </div>

                    ))}
                </Slider>
            </div>
            <div>
                <Image src={ImageBorder} alt="slide" className="w-[100%]" />
            </div>

        </div>

    )
}
export default PartOneHomePage

